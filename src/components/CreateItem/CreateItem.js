import React, {useState} from 'react';
import Parse from "parse"
const CreateItem = () => {
  const [label,setlabel] = useState();
  const handlelabel = (e) => {
    setlabel(e.target.value)
  }
  const handlesave = () => {
    const ItemClass = Parse.Object.extend('Item');
    const newItem = new ItemClass();

    newItem.set('label', label);

    newItem.save().then(
        (result) => {
          setlabel("")
          //console.log(result);
        },
        (error) => {
          console.error(error);
        }
    );
  }
  return <div className={"CreateItem"} id={"CreateItem"}>
    <input onChange={handlelabel} defaultValue={label}/>
    <button onClick={handlesave}>save</button>
  </div>;
};

export default CreateItem;
