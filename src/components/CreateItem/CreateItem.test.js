import React from 'react';
import { shallow } from 'enzyme';
import CreateItem from './CreateItem';

describe('CreateItem', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<CreateItem />);
    expect(wrapper).toMatchSnapshot();
  });
});
