import React from 'react';
import { shallow } from 'enzyme';
import UpdateItem from './UpdateItem';

describe('UpdateItem', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<UpdateItem />);
    expect(wrapper).toMatchSnapshot();
  });
});
