import React, {useEffect, useState} from 'react';
import Parse from "parse"
const UpdateItem = (props) => {
  const [id,setid] = useState();
  const [item,setitem] = useState();
  const [label,setlabel] = useState();
  const handlelabel = (e) => {
    setlabel(e.target.value)
  }
  const handleupdate = () => {
    const ItemClass = Parse.Object.extend('Item');
    const query = new Parse.Query(ItemClass);
    query.get(item.id).then((object) => {
      object.set('label', label);
      object.save().then((response) => {
        //console.log(response);
      }, (error) => {
        console.error(error);
      });
    });
  }
  useEffect(()=>{
    if (props.item){
      setitem(props.item)
    } else {setitem({})}
  },[])
  return <div className={"UpdateItem"} id={"UpdateItem"}>
    <input onChange={handlelabel} defaultValue={item?.get("label")||""}/>
    <button onClick={handleupdate}>save</button>

  </div>;
};

export default UpdateItem;
