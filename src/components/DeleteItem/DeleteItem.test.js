import React from 'react';
import { shallow } from 'enzyme';
import DeleteItem from './DeleteItem';

describe('DeleteItem', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<DeleteItem />);
    expect(wrapper).toMatchSnapshot();
  });
});
