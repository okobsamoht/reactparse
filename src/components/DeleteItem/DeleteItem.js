import React, {useEffect, useState} from 'react';
import Parse from "parse"
const DeleteItem = (props) => {
  const [item,setitem] = useState();
  const handledelete = () => {
    const ItemClass = Parse.Object.extend('Item');
    const query = new Parse.Query(ItemClass);
    query.get(item.id).then((object) => {
      object.destroy().then((response) => {
        //console.log(response);
      }, (error) => {
        console.error(error);
      });
    });
  }
  useEffect(()=>{
    if (props.item){
      setitem(props.item)
    }
  },[])
  return <div className={"DeleteItem"} id={"DeleteItem"}>
    <button onClick={handledelete}>x</button>
  </div>;
};

export default DeleteItem;
