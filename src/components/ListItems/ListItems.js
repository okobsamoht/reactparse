import React, {useEffect, useState} from 'react';
import Parse from "parse"
import DeleteItem from "../DeleteItem";
import UpdateItem from "../UpdateItem";

const ListItems = () => {
  const [list,setlist] = useState([]);
  useEffect(()=>{
    if (Parse.User.current()){
      const client = new Parse.LiveQueryClient({
        applicationId: 'eQFBIpx7dCpaKS4EM5EZn0Lhz0WZegTPMakZEj67',
        serverURL: 'wss://reactparse.back4app.io',
        javascriptKey: 'bJC2UlmiSBZmI3HoOR6jif38C2yH59jENeOkRYSd'
      });
      client.open();

      const query = new Parse.Query('Item');
      const subscription = client.subscribe(query);

      subscription.on('open', item => {
        subscription.query.find().then(e=>setlist(e));
      });
      subscription.on('create', item => {
        subscription.query.find().then(e=>setlist(e));
      });
      subscription.on('update', item => {
        subscription.query.find().then(e=>setlist(e));
      });
      subscription.on('delete', item => {
        subscription.query.find().then(e=>setlist(e));
      });

      // const ItemClass = Parse.Object.extend('Item');
      // const query = new Parse.Query(ItemClass);
      // query.find().then((results) => {
      //   setlist(results)
      //   console.log(results);
      // }, (error) => {
      //   console.error(error);
      // });
    }
  },[])
  return <div className={"ListItems"} id={"ListItems"}>
    <ul>
      {list.map(i=>(<li key={i.id}>{i.get("label")} <DeleteItem item={i}/> <UpdateItem item={i}/></li>))}
    </ul>
  </div>;
};

export default ListItems;
