import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Parse from "parse"
Parse.serverURL="https://parseapi.back4app.com/"
Parse.liveQueryServerURL="wss://reactparse.back4app.io"
Parse.initialize("eQFBIpx7dCpaKS4EM5EZn0Lhz0WZegTPMakZEj67","bJC2UlmiSBZmI3HoOR6jif38C2yH59jENeOkRYSd")
if (!Parse.User.current()){
    Parse.User.logIn("reactparse","reactparse").then(u=>console.log(u))
}

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
