import React from 'react';
import logo from './logo.svg';
import './App.css';
import Parse from "parse"
import CreateItem from "./components/CreateItem";
import ListItems from "./components/ListItems";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <hr/>
        <CreateItem/>
        <ListItems/>
      </header>
    </div>
  );
}

export default App;
